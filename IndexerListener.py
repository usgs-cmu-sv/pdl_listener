#! /usr/bin/env python

import os
import sys
import datetime
import StringIO

# add ProductClient directory to path
sys.path.append(os.path.join('lib', 'ProductClient'))
sys.path.append(os.path.join('utils',''))

# import IndexerAction, which parses ExternalIndexerListener arguments
from ExampleListener import IndexerAction
from activemq import *
from polygon import bounding_region
from utils import *

if __name__ == '__main__':
    # write data to a log file
    logfile = os.path.join('data', os.path.basename(sys.argv[0]) + '.log')
    f = open(logfile, 'ab+')
    # current time
    f.write('# ' + datetime.datetime.now().isoformat() + '\n');
    # command line arguments
    f.write('# arguments = ' + ' '.join(sys.argv) + '\n');
    # parse command line arguments
    action = IndexerAction.getIndexerAction()
    # output parsed action
    f.write('action=' + action.action + '\n')
    # output associated product
    product = action.product
    product.display(f)

    # ignore deleted product/events notifications
    if product.status == 'DELETE':
        f.write("\n")
        f.close()
        sys.exit()

    # open region-def file and load
    bbox = load_regions()

    srcCode = product.code
    props = product.properties
    mag = float(props['magnitude'])
    latitude = props['latitude']
    longitude = props['longitude']

    # filter by magnitude and region in index_region.ini
    if filter_by(bbox, mag, latitude, longitude):
        dispatch_notif(srcCode, longitude, latitude)
        f.write('activemq notified: ' + srcCode + ' ' + str(mag) + '\n')

    f.write('\n')
    f.close()
