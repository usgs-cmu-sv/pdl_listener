Indexer Listener Example
==============================

Run a python ExternalIndexerListener configured to process indexer changes.


Getting Started
---------------

- Run `./init.sh start`

- Watch the log files created in the `data` directory, in particular `data/IndexerListener.py.log` that is created once the first product is processed.
