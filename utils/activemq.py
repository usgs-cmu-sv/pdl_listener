#! /usr/bin/env python

# ActiveMQ broker helper functions using stomp
#
# Author: Daniel Chen

import stomp
from stomp import *

def dispatch_notif(srcCode, longitude, latitude):
    c = stomp.Connection([('10.214.2.47', 61613)])
    c.start()
    c.connect('admin', 'admin', wait=True)
    c.send('/queue/PDL', '{\"eventId\":\"' + srcCode + '\",\"regionType\":\"WC Circular 1994\",\"centerType\":\"Epicenter\",\"radius\":20.0,\"circleCenter\":{\"lat\":0.0,\"lon\":0.0,\"depth\":1000.0},\"minLocation\":{\"lat\":0.0,\"lon\":0.0,\"depth\":0.0},\"maxLocation\":{\"lat\":0.0,\"lon\":0.0,\"depth\":700.0},\"persistent\":true,\"emulate\":false,\"dataMaxDays\":0.0,\"dataMinDays\":0.0,\"minA\":-4.5,\"maxA\":-0.5,\"minP\":0.98,\"maxP\":0.98,\"minC\":0.018,\"maxC\":0.018,\"b\":1.0,\"g\":0.25,\"h\":1.0,\"magCat\":4.5}')
    c.disconnect()
    
