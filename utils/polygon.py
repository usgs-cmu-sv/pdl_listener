#! /usr/bin/env python

# Polygon class helper for definitions of bounding regions
#
# Author: Daniel Chen

class bounding_region:
    def __init__(self, mag=None, lats=None, longs=None):
        self.mag = mag
        self.lats = lats
        self.longs = longs
    
    # This is one of the simpler implementation of Jordan's curve test,
    # or ray test. It basically selects a random point outside the
    # region defined by lats and longs and uses this it as a starting
    # point and go towards the point we are testing while counting
    # the number of sides of the polygon this ray has penetrate. If it 
    # is even, the point of interest lies outside the defined region. 
    # If it is odd, it is within the region.
    # 
    # Source:
    # https://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html     
    def contains(self, latitude, longitude):
        if not self.lats: 
            return True
        in_region = False

        if len(self.longs) == 4:
            #if ((latitude < max(self.lats)) and (latitude > min(self.lats)) and 
            #    (longitude < max(self.longs)) and (longitude < min(self.longs))):
            if ((latitude < max(self.lats)) and (latitude > min(self.lats)) and 
                (longitude < max(self.longs)) and (longitude > min(self.longs))):
                in_region = True
        else:
            j = len(self.longs) - 1
            for i in range(len(self.lats)):
                if (((self.lats[i] > latitude) != (self.lats[j] > latitude)) and 
                    (longitude < ((self.longs[j]-self.longs[i])*(latitude-self.lats[i])/ 
                    (self.lats[j]-self.lats[i]) + self.longs[i]))):
                        in_region = not in_region
                j = i
        return in_region
