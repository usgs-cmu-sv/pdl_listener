#! /usr/bin/env python

# Utility functions for setting up filter
# mechanism for PDL notificaitons
#
# Author: Daniel Chen

import os
import sys
from polygon import bounding_region

def filter_by(bbox, magnitude, latitude, longitude):
    result = False
    for reg in bbox:
        if (magnitude > reg.mag and 
            reg.contains(float(latitude), float(longitude))):
            return True
        else: 
            result = False
    return result

def load_regions():
    if not os.path.isfile('index_filter.ini'):
        return bounding_region()

    regions = []
    reg_f = open('index_filter.ini', 'r')
    for line in reg_f.readlines():
        l = line.strip()
        if l and not l.startswith('#'):
            e = line.split()
            name, mag = e[0], float(e[1])
            lats = []
            longs = []
            for i in range(2, len(e)):
                if i % 2: longs.append(float(e[i]))
                else: lats.append(float(e[i]))
            region = bounding_region(mag, lats, longs)
            regions.append(region)

    reg_f.close()
    return regions
